<?php
// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}



?>

<div class="card">
    <div class="card-header">
        <h5><?=isset($h1)?$h1:'Сообщение'?></h5>
    </div>
    <div class="card-body">
        <p>
            <?=isset($message)?$message:'Сообщение не определено'?>
        </p>
    </div>
</div>
