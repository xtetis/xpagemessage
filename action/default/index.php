<?php

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}




$params = \xtetis\xpagemessage\models\PageMessageModel::getParams();
        
if ((isset($params['name'])) && (strlen($params['name'])))
{
    // Устанавливаем Name страницы
    \xtetis\xengine\helpers\SeoHelper::setPageName($params['name']);
}
else
{
    // Устанавливаем Name страницы, если не установлен другой
    \xtetis\xengine\helpers\SeoHelper::setPageName('Сообщение пользователю', false);
}


if ((isset($params['title'])) && (strlen($params['title'])))
{
    // Устанавливаем Title страницы
    \xtetis\xengine\helpers\SeoHelper::setTitle($params['title']);
}
else
{
    // Устанавливаем Title страницы, если не установлен другой
    \xtetis\xengine\helpers\SeoHelper::setTitle('Сообщение пользователю', false);
}

\xtetis\xengine\App::getApp()->setParam('layout', 'list');

// Рендерим текущую страницу
echo \xtetis\xengine\App::getApp()->renderCurrentPage(
    $params,
);
