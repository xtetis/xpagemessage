<?php

namespace xtetis\xpagemessage;

/**
 * Модуль страницы с сообщением для пользователя с единым урлом
 */
class Component extends \xtetis\xengine\models\Component
{

    /**
     * Рендер страницы с сообщением для пользователя единым урлом
     */
    public static function renderPageMessage($params = [])
    {
        $session_params = \xtetis\xpagemessage\models\PageMessageModel::getParams();

        $h1      = '';
        $message = '';

        if (isset($params['name']))
        {
            // Устанавливаем Name страницы
            \xtetis\xengine\helpers\SeoHelper::setPageName($params['name']);
            $h1 = $params['name'];
        }
        else
        {
            if ((isset($session_params['name'])) && (strlen($session_params['name'])))
            {
                // Устанавливаем Name страницы
                \xtetis\xengine\helpers\SeoHelper::setPageName($session_params['name']);
                $h1 = $params['name'];
            }
            else
            {
                // Устанавливаем Name страницы, если не установлен другой
                \xtetis\xengine\helpers\SeoHelper::setPageName('Сообщение пользователю', false);
                $h1 = 'Сообщение пользователю';
            }
        }

        if (isset($params['title']))
        {
            // Устанавливаем Title страницы
            \xtetis\xengine\helpers\SeoHelper::setTitle($params['title']);
        }
        else
        {
            if ((isset($session_params['title'])) && (strlen($session_params['title'])))
            {
                // Устанавливаем Title страницы
                \xtetis\xengine\helpers\SeoHelper::setTitle($session_params['title']);
            }
            else
            {
                // Устанавливаем Title страницы, если не установлен другой
                \xtetis\xengine\helpers\SeoHelper::setTitle('Сообщение пользователю', false);
            }
        }

        if (isset($params['message']))
        {
            $message = $params['message'];
        }
        else
        {
            if ((isset($session_params['message'])) && (strlen($session_params['message'])))
            {
                $message = $session_params['message'];
            }
            else
            {
                $message = 'Текст сообщения пользователю';
            }
        }


        echo \xtetis\xengine\helpers\RenderHelper::renderLayout(
            self::renderBlock(
                'default/index',
                [
                    'message' => $message,
                    'h1'      => $h1,
                ]
            )
            //\xtetis\xpagemessage\controllers\PageMessageController::renderPageMessage()
        );
        exit;
    }

    /**
     * Устанавливаем параметры для отображения страницы с сообщением для пользователя с единым урлом
     *
     * @param array $params
     */
    public static function setPageMessageParams($params = [
        'message' => '',
        'name'    => '',
        'title'   => '',
    ])
    {
        \xtetis\xpagemessage\models\PageMessageModel::setParams($params);
    }

    /**
     * Страница кастомной ошибки
     */
    public static function customDie($message)
    {
        self::setPageMessageParams([
            'message' => $message,
            'name'    => 'Ошибка',
            'title'   => 'Ошибка',
        ]);
        self::renderPageMessage();
        exit;
    }
}
