<?php

namespace xtetis\xpagemessage\models;

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

class PageMessageModel extends \xtetis\xengine\models\Model
{
    /**
     * @var array
     */
    public $params = [];

    /**
     * Устанавливаем параметры для отображения страницы с сообщением для пользователя с единым урлом
     * 
     * @param array $params
     */
    public static function setParams($params = [
        'message' => '',
        'name'    => '',
        'title'   => ''
    ])
    {
        $model = self::getModel(
            '',
            [],
            true
        );

        $allow_create_params = [
            'message',
            'h1',
            'title',
        ];

        foreach ($allow_create_params as $allow_create_params_item)
        {
            if (isset($params[$allow_create_params_item]))
            {
                $model->params[$allow_create_params_item] = trim($params[$allow_create_params_item]);
            }
        }

        $model->saveModel();
    }

    /**
     * @return mixed
     */
    public static function getParams()
    {
        $model = self::getModel(
            '',
            [],
            false
        );

        if (!$model)
        {
            return [];
        }

        return $model->params;
    }


    
}
