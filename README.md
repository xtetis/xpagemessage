# xtetis\xpagemessage
## Модуль для страницы с сообщением для пользователя для движка xEngine 
Урл страницы один и тот же. Используется для отображения пользователю сервисных сообщений. Например, текст об успешной регистрации, активации акааунта и т.д.

Powered by xTetis


---
## Установка модуля

 - Если Вы используете xEngine (https://bitbucket.org/xtetis/xengine/), то он содержит в ноде require файла composer.json, потому будет установлен автоматически.
```sh
"xtetis/xpagemessage": "dev-master",
```
 - После установки модуля необходимо прописать следующие константы в параметрах engine/config 

| Константа | Тип     | Пример                | Описание                |
| :-------- | :------- | :------------------------- | :------------------------- |
| `COMPONENT_URL_LIST` | `строка, сериализованный массив` | define("COMPONENT_URL_LIST", serialize(['pagemessage' => 'xtetis\xpagemessage'])); | Список урлом модулей xEngine |
| `DEFAULT_LAYOUT` | `строка` | define("DEFAULT_LAYOUT", 'page'); | Шаблон по-умолчанию |


## Использование в коде

Для создания контента, который будет отображен на странице с сообщением
```php
    // Устанавливаем параметры для отображения страницы с сообщением для пользователя с единым урлом
    \xtetis\xpagemessage\Component::setPageMessageParams([
        'message' => $message,
        'name'    => 'Письмо с подтверждением',
        'title'   => 'Вам отправлено письмо',
    ]);
```

Если необходимо определить другую верстку страницы (использовать другой блок) - можно передать параметр

Для создания контента, который будет отображен на странице с сообщением
```php
    // Устанавливаем параметры для отображения страницы с сообщением для пользователя с единым урлом
    \xtetis\xpagemessage\Component::setPageMessageParams([
        'message' => $message,
        'name'    => 'Письмо с подтверждением',
        'title'   => 'Вам отправлено письмо',
        'block'   => 'blocks/custom_pagemessage'
    ]);
```

В коде блока для использования кастомного шаблона нужно установить параметр layout
```php
    // Установка кастомного шаблона
    \xtetis\xengine\App::setParam('layout', 'layout_name');
```


Генерация урла страницы для отображения сообщения
```php
   // Урл страницы для сообщения пользователю после регистрации
    $url_pagemessage = \xtetis\xpagemessage\Component::getModuleUrl('pagemessage');
```

Также можно отобразить сообщение на текущей странице (например, сообщение об ошибке)
```php
    // Метод, который выполняется когда нужно прекратить выполнение 
    // и показать пользователю сообщение об ошибке
    define('CUSTOM_DIE', '\xtetis\xcms\Component::cmsCustomDie');
    
    /**
     * Страница кастомной ошибки
     */
    public static function cmsCustomDie($message)
    {
        \xtetis\xpagemessage\Component::setPageMessageParams([
            'message'=>$message,
            'name'=>'Ошибка',
            'title'=>'Ошибка',
        ]);
        \xtetis\xpagemessage\Component::renderPageMessage();
        exit;
    }
```

## Обратная связь

Для связи с автором автором
 - skype: xtetis
 - telegram: @xtetis
